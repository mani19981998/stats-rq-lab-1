## jhnoll@gmail.com
ROOT:=$(shell $(HOME)/bin/findup .sgrc)
SOURCE_DIR=.
BIB=references.bib
include $(ROOT)/tools/Makefile.in

# This is used by tools/Makefile.in to create 'all:' target.
INSTALL_OBJECTS=README.md instructions.md instructions.txt instructions.pdf instructions.html rubric.xlsx

all: $(INSTALL_OBJECTS)

README.txt: ${TOOLS.dir}/templates/lab-README.md
	$(PP) -Dcomponent=${*} $(META_DATA) $< | $(PANDOC) --standalone -t plain -o $@

README.md: ${TOOLS.dir}/templates/lab-README.md
	$(PP) $< > $@

rubric.xlsx: $(ROOT)/grades/research-question/rubric.xlsx
	cp $< $@


include $(ROOT)/tools/Make.rules
clean:
	rm README.md instructions.md *.html *.pdf


